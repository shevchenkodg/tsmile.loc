<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UploadCsvRequest;
use App\Products;
use App\Warehouses;
use File;

class HomeController extends Controller
{
    public function getIndex() {

        $dbProducts = Products::with('productWarehouses')->get();
        // Sum quantity and warehouses by products
        $products = [];
        foreach ($dbProducts as $product) {
            $qty = 0;
            $warehouse = null;
            foreach ($product->productWarehouses as $productWarehouse) {
                $qty += $productWarehouse->qty;
                if($warehouse) {
                    $warehouse .= ',';
                }
                $warehouse .= $productWarehouse->warehouse;
            }
            $products[] = [
                'product_name' => $product->product_name,
                'qty'          => $qty,
                'warehouse'    => $warehouse
            ];
        }

        return view('home', compact(['products']));
    }

    public function postIndex(UploadCsvRequest $request) {

        $file = $request->file;

        // Parse CSV file by line
        // $row - string number
        $row = 1;
        if (($handle = fopen($file->getPathname(), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $num = count($data); // Count of values in string
                for ($c=0; $c < $num; $c++) {
                    // Going first CSV row. Parsing product info
                    if($data[$c] && $c==0) {
                        // Product name
                        $csvProductName = $data[$c];
                    }
                    if($data[$c] && $c==1) {
                        // Qty
                        $csvQty = $data[$c];
                    }
                    if($data[$c] && $c==2) {
                        // Warehouse
                        $csvWarehouse = $data[$c];
                    }
                }
                // Get current product info from DB
                $dbProduct = Products::where('product_name', $csvProductName)->first();
                if ($dbProduct) {
                    $dbProductID = $dbProduct->id;
                    // Info is present. Update it.
                    // Check for new warehouse
                    $dbWarehouse = Warehouses::where('product_id', $dbProductID)->where('warehouse', $csvWarehouse)->first();
                    if ($dbWarehouse) {
                        // Warehouse present. Update quantity
                        // Calculate new quantity
                        $newQty = $dbWarehouse->qty + $csvQty;
                        if($newQty > 0) {
                            // If quantity > 0
                            Warehouses::where('product_id', $dbProductID)->where('warehouse', $csvWarehouse)->update([
                                'qty' => $newQty
                            ]);
                        }
                        else {
                            // Product in the warehouse no more
                            Warehouses::where('product_id', $dbProductID)->where('warehouse', $csvWarehouse)->delete();
                            // Check, if present product in warehouses
                            $countWarehouses = Warehouses::where('product_id', $dbProductID)->count();
                            if(!$countWarehouses) {
                                // If no more product in warehouses, delete from DB
                                Products::where('id', $dbProductID)->delete();
                            }
                        }
                    }
                    else {
                        // Warehouse not present. Insert warehouse and quantity
                        Warehouses::insert([
                            'product_id' => $dbProductID,
                            'qty'        => $csvQty,
                            'warehouse'  => $csvWarehouse
                        ]);
                    }
                }
                else {
                    // Info not present. Insert it.
                    // Insert product name
                    $dbProduct = Products::create([
                        'product_name' => $csvProductName
                    ]);
                    // Insert qty and warehouse
                    Warehouses::insert([
                        'product_id' => $dbProduct->id,
                        'qty' => $csvQty,
                        'warehouse' => $csvWarehouse
                    ]);
                }
                $row++; // Number of string
            }
            fclose($handle);
        }

        return redirect()->back();
    }
}