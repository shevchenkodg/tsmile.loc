<!DOCTYPE html>
<html>
    <head>
        <title>Тest Smile</title>
        <link rel="stylesheet" href="/template/bootstrap/css/bootstrap.min.css">
    </head>
    <body>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Table Of Product Balances By Warehouses</h3>
            </div>
            <div class="panel-body">
                @if(count($products))
                <table class="table">
                    <thead>
                    <tr>
                        <th>Product name</th>
                        <th>Qty</th>
                        <th>Warehouse</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                        <tr>
                            <td>{!! $product['product_name'] !!}</td>
                            <td>{!! $product['qty'] !!}</td>
                            <td>{!! $product['warehouse'] !!}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                No products. You can upload it from CSV.
                @endif
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Upload CSV File</h3>
            </div>
            <div class="panel-body">
                {!! Form::open(['class' => 'navbar-form navbar-left', 'files' => true]) !!}
                    <div class="form-group">
                        <input type="file" name="file" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-default">Upload</button>
                {!! Form::close() !!}
            </div>
        </div>
    </body>
</html>
